import { Component, OnInit } from '@angular/core';
import { StrategyID } from '../strategyid';
import { ApiService } from '../api.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {LessThan} from '../helper/lessthan';

@Component({
  selector: 'app-movingaverages-ui',
  templateUrl: './movingaverages-ui.component.html',
  styleUrls: ['./movingaverages-ui.component.css']
})
export class MovingaveragesUIComponent implements OnInit {
  submitted = false;
  angForm: FormGroup;
  stocknames;
  date = new Date();
  day: any;
  month: any;
  strategyid : StrategyID = new StrategyID(1,"Moving Averages");
  constructor(private apiService: ApiService,
              private fb: FormBuilder) { }
  ngOnInit() {
    this.day = this.date.getDate();
    this.month = this.date.getMonth()+2;
    if(this.day<10){
      this.day = "0" + this.day;
    }

    if(this.month<10){
      this.month = "0" + this.month;
    }
    this.apiService.getStocknames().subscribe((data)=>{
      this.stocknames = data;
    });
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      stock: ['', Validators.required ],
      shortAvgTime: ['', Validators.required ],
      longAvgTime: ['', Validators.required ],
      profit: ['', Validators.required ],
      loss: ['', Validators.required ],
      volume: ['', Validators.required ],
      stdDeviationTime: ['', Validators.required ],
      date: [this.date.getFullYear() + "-" + this.day + "-" + this.month],
      time: [this.date.getTime()],
      strategyId: [1],
      active: [true]
    },
    {
      validator: LessThan("shortAvgTime", "longAvgTime")
    });
  }
  get f(){
    return this.angForm.controls;
  }
  postStrategy(){
    this.submitted = true;
    if(this.angForm.invalid){
      return;
    }
    this.angForm.value["stdDeviationTime"]*=1000;
    this.apiService.postMovingAvgInfo(this.angForm.value).subscribe((data)=>{
      this.angForm.reset();
    });
  }
  getStrategy(){
    this.apiService.getData(1).subscribe((data)=>{
      console.log(data);
    });
  }
}