import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovingaveragesUIComponent } from './movingaverages-ui.component';

describe('MovingaveragesUIComponent', () => {
  let component: MovingaveragesUIComponent;
  let fixture: ComponentFixture<MovingaveragesUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovingaveragesUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovingaveragesUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
