import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BollingerUIComponent } from './bollinger-ui/bollinger-ui.component';
import { MovingaveragesUIComponent } from './movingaverages-ui/movingaverages-ui.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { TransactionUIComponent } from './transaction-ui/transaction-ui.component';
import {RouterModule, Routes} from '@angular/router';
import { StrategiesUIComponent } from './strategies-ui/strategies-ui.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    BollingerUIComponent,
    MovingaveragesUIComponent,
    DashboardComponent,
    TransactionUIComponent,
    StrategiesUIComponent
  ],
  imports: [
    BrowserModule,    
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
