export class StrategyID{
    constructor(
        public id: number = 1,
        public strategyName: string
    ){}
}