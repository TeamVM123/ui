import {StrategyID} from "./strategyid";

export class BollingerInfo{
    constructor(
        public active: boolean,
        public strategyId: number,
        public stdDeviationTime: number,
        public numberofOccur: number,
        public name: string,
        public stock: string,
        public profit: number,
        public loss: number,
        public date: string,
        public time: number,
        public volume: number
    ){}
}