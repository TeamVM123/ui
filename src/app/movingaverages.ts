import {StrategyID} from "./strategyid";

export class MovingAvgInfo{
    constructor(
        public strategyid: StrategyID,
        public stdDeviationTime: number,
        public numberofOccur: number,
        public shortAvgTime: number,
        public longAvgTime: number,
        public name: string,
        public stock: string,
        public profit: number,
        public loss: number,
        public date: string,
        public time: number,
        public volume: number
    ){}
}