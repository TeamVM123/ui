import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BollingerInfo } from './bollinger';
import { MovingAvgInfo } from './movingaverages';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }
  
  /* GET from database */
  public getData(num:number){
    return this.httpClient.get(`http://172.31.14.93:8080/api/Strategy/1`);
  }

  public getStratData(){
    return this.httpClient.get(`http://172.31.14.93:8080/api/StrategyInfo/`)
  }

  public getStratDataByName(name){
    return this.httpClient.get(`http://172.31.14.93:8080/api/StrategyInfo/${name}`);
  }

  /* GET list of company names of every stock from data feed URL */
  public getStocknames(){
    return this.httpClient.get(`http://nyc31.conygre.com:31/Stock/getSymbolListOrderedBySymbol`);
  }

  /* POST Bollinger parameter values into database */
  public postBollingerInfo(bollinger: BollingerInfo){
    console.log("ok");
    let headers = {headers: new HttpHeaders({'Content-Type':'application/json'})};
   return this.httpClient.post(`http://172.31.14.93:8080/api/StrategyInfo`, bollinger, headers); 
  }

  /* POST Moving Avg parameter values into database */
  postMovingAvgInfo(movingAvgInfo: MovingAvgInfo) {
    return this.httpClient.post(`http://172.31.14.93:8080/api/StrategyInfo`, movingAvgInfo);
  }

  /* DELETE strategy info instance */
  deleteStrategy(name){
    return this.httpClient.delete(`http://172.31.14.93:8080/api/StrategyInfo/${name}`);
  }

  activateStrategy(name){
    this.getStratDataByName(name).subscribe((data)=>{
      data["active"] = !data["active"];
        this.httpClient.put(`http://172.31.14.93:8080/api/StrategyInfo/`, data).subscribe((d)=>{
          console.log("hi");
        });
    });
    
  }

  /* GET strategy info data of one particular person */
  getStratDataOf(name){
    return this.httpClient.get(`http://172.31.14.93:8080/api/StrategyInfo/${name}`)
  }

  getTransactions(id:number){
    return this.httpClient.get(`http://172.31.14.93:8080/api/transaction/after/${id}`)
  }

}

