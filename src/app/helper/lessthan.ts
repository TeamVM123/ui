import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function LessThan(shortAvg: string, longAvg: string) {
    return (formGroup: FormGroup) => {
        const short = formGroup.controls[shortAvg];
        const long = formGroup.controls[longAvg];

        if(long.errors && !long.errors.lessThan){
            return;
        }

        // set error on matchingControl if validation fails
        if (parseInt(short.value) >= parseInt(long.value)) {
            long.setErrors({ lessThan: true });
        } else {
            long.setErrors(null);
        }
    }
}