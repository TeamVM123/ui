import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { BollingerInfo } from '../bollinger';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-bollinger-ui',
  templateUrl: './bollinger-ui.component.html',
  styleUrls: ['./bollinger-ui.component.css']
})
export class BollingerUIComponent implements OnInit {
  submitted = false;
  angForm: FormGroup;
  stocknames;
  date: Date = new Date();
  day: any;
  month: any;
  constructor(private apiService: ApiService,
              private fb: FormBuilder) { }
  ngOnInit() {
    this.day = this.date.getDate();
    this.month = this.date.getMonth()+2;
    if(this.day<10){
      this.day = "0" + this.day;
    }
    if(this.month<10){
      this.month = "0" + this.month;
    }
    this.apiService.getStocknames().subscribe((data)=>{
      this.stocknames = data;
    });
    this.createForm();
  }
  createForm(){
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      stock: ['', Validators.required ],
      amount: [''],
      profit:['', Validators.required],
      loss:['', Validators.required],
      volume:['', Validators.required],
      stdDeviationTime:['', Validators.required],
      date: [this.date.getFullYear()+"-"+this.day+"-"+this.month],
      time: [this.date.getTime()],
      strategyId: [0],
      active: [true]
    })
  }

  get f(){
    return this.angForm.controls;
  }

  postStrategy(){
    this.submitted = true;
    if(this.angForm.invalid){
      return;
    }
    this.angForm.value["stdDeviationTime"]*=1000;
    this.apiService.postBollingerInfo(this.angForm.value).subscribe((data)=>{
      alert("submitted");
    });
  }
  getStrategy(){
    this.apiService.getData(1).subscribe((data)=>{
      console.log(data);
    });
  }
}