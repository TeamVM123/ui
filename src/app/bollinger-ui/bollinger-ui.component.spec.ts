import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BollingerUIComponent } from './bollinger-ui.component';

describe('BollingerUIComponent', () => {
  let component: BollingerUIComponent;
  let fixture: ComponentFixture<BollingerUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BollingerUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BollingerUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
