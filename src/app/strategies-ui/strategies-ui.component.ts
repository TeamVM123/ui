import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-strategies-ui',
  templateUrl: './strategies-ui.component.html',
  styleUrls: ['./strategies-ui.component.css']
})
export class StrategiesUIComponent implements OnInit {
  data;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getStrategyData();
  }

  getStrategyData(){
    this.apiService.getStratData().subscribe((data:any)=>{
      data.forEach(function(v){ delete v["numberofOccur"] });
      data.forEach(function(v){ v["date"] = v["date"].substring(0,10)});
      this.data = data;
    });
  }

  deleteStrategy(row){
    this.apiService.deleteStrategy(row["name"]).subscribe((data)=>{
      console.log(row["name"] + " has been deleted.");
      alert("Deleted.");
    })
  }

  activateStrategy(row, active){
    let negate : any;
    negate = !active;
    row["active"] = !row["active"];
    document.getElementById(row["name"]).innerHTML = negate;
    this.apiService.activateStrategy(row["name"]);
  }
}
