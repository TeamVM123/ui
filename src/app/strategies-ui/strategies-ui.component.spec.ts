import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategiesUIComponent } from './strategies-ui.component';

describe('StrategiesUIComponent', () => {
  let component: StrategiesUIComponent;
  let fixture: ComponentFixture<StrategiesUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategiesUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategiesUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
