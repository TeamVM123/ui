import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, interval } from 'rxjs';

@Component({
  selector: 'app-transaction-ui',
  templateUrl: './transaction-ui.component.html',
  styleUrls: ['./transaction-ui.component.css']
})
export class TransactionUIComponent implements OnInit {
  currTransNumber = 0;
  allData = [];
  startingAmounts ={}; // starting amounts
  currentAmounts ={}; // current amounts, to be compared to with starting amount
  profitLoss ={}; // 
  currentVolume ={}; // keeps track of each strategy's current volume
  startingCash = {};
  currentCash = {};
  profit = {}; 
  globalStart = 0;
  transactions;
  name;
  lineChart;
  config = {
    type: 'line', 
    data: {
      labels:[],
      datasets:[],
    },
    options: {
    tooltips:{
      callbacks: {
        title: function(tooltipItems, data) {
          return '';
        }
      }
    },
    scaleShowLabels: false,
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero:true
          }
      }]
    }
    }
  };
  constructor(private httpClient: HttpClient, private apiService: ApiService) { }

  ngOnInit() {
    this.start();
  }

  start(){
    interval(10000).subscribe(data =>{ 
      
      let strategyInfo = this.httpClient.get('http://172.31.14.93:8080/api/StrategyInfo/');
      let tran = this.httpClient.get(`http://172.31.14.93:8080/api/transaction/after/${this.currTransNumber}`);
      
      forkJoin([strategyInfo, tran]).subscribe((res: any) => {
        res[1].sort((a,b)=>{return a.id - b.id});
        let lastEleIndex = res[1].length-1;

        // only update stuff when there is a new array
        if(res[1][lastEleIndex]){
          // current number = 0, get the id of the last element of the array, and query that in the next interval
          this.currTransNumber = res[1][lastEleIndex].id+1;

          for(var i = 0; i < res[1].length; i++){
            let strategyName = res[1][i].strategyName;
            if(!this.allData[strategyName]){
              this.allData[strategyName] = {};
            }

            // if price is undefined, then this is the first time querying... so start price
            if(!this.allData[strategyName]["price"] && !this.startingAmounts[strategyName]){
              let startPrice = res[1][i]["price"];
              let startAmount = this.getStartFactor(res[0], strategyName, "amount");
              let volume = this.getStartFactor(res[0], strategyName, "volume")*2;
              this.startingAmounts[strategyName] = startAmount + volume*startPrice;
              this.currentAmounts[strategyName] = startAmount + volume*startPrice;
              this.currentVolume[strategyName] = volume;
              this.startingCash[strategyName] = startAmount;
              this.currentCash[strategyName] = startAmount;
              this.allData[strategyName]["price"] = [];
              this.allData[strategyName]["stratId"] = res[1][i].strategy;
            }
            if(!this.allData[strategyName]["time"]){
              this.allData[strategyName]["time"] = [];
            }
            // Calculate and store the profit/loss margins here
            // 3000 + 1.14 * 1000 = 4140
            let currPrice = res[1][i]["price"]*res[1][i]["quantity"];
            let buy = res[1][i]["buy"];
            let newAmount;
            let newVolume;
            
            if(buy){
              newVolume = this.currentVolume[strategyName] + res[1][i]["quantity"];
              this.currentVolume[strategyName] = newVolume;
              this.currentCash[strategyName] = this.currentCash[strategyName] - currPrice;
              newAmount = this.currentCash[strategyName] + newVolume * res[1][i]["price"];
              this.currentAmounts[strategyName] = newAmount;
            }
            else{
              newVolume = this.currentVolume[strategyName] - res[1][i]["quantity"];
              this.currentVolume[strategyName] = newVolume;
              this.currentCash[strategyName] = this.currentCash[strategyName] + currPrice;
              newAmount = this.currentCash[strategyName] + newVolume * res[1][i]["price"];
              this.currentAmounts[strategyName]= newAmount;
            }
            let loss = this.getStartFactor(res[0], strategyName, "loss");
            let profit = this.getStartFactor(res[0], strategyName, "profit");
            console.log("---");
            console.log(res[1][i]);
            let exit = this.checkExitCondition(loss, profit, this.startingAmounts[strategyName], this.currentAmounts[strategyName]);
            console.log(res[0]);
            this.handleExit(exit, strategyName, res[0]);
            this.allData[strategyName]["price"].push(newAmount);
            this.allData[strategyName]["time"].push(res[1][i]["time"]);
          }
          this.lineChart = new Chart('lineChart', this.config);
          this.buildChart();
        }
      });
    });
  }

  buildChart(){
    for(var key in this.allData){
      
      this.sortTimes(this.allData[key].time);
      this.config.data.labels = this.config.data.labels.concat(this.allData[key].time);
      this.sortTimes(this.config.data.labels);
      let newObj = {
        key:key,
        label: [key, this.allData[key].stratId===0 ? "Bollinger" : "Moving Average"],
        borderColor: '#'+Math.random().toString(16).slice(-6),
        data: []
      };
      for(var i = 0; i < this.allData[key].time.length; i++){
        let a = {};
        a["x"] = this.allData[key].time[i];
        a["y"] = this.allData[key].price[i];
        newObj.data.push(a);
      }
      this.sortTimesObj(newObj.data);
      let exists = this.keyAlreadyExists(key);
      if(!exists){
        this.config.data.datasets.push(newObj);
      }

      this.allData[key].time.length=0;
      this.allData[key].price.length=0;
    }
    this.lineChart.update();
  }

  keyAlreadyExists(key:string){
    for(var i = 0; i < this.config.data.datasets.length; i++){
      if(this.config.data.datasets[i].key === key){
        for(var j = 0; j < this.allData[key].time.length; j++){
          let a = {};
          a["x"] = this.allData[key].time[j];
          a["y"] = this.allData[key].price[j];
          this.config.data.datasets[i].data.push(a);
        }
        this.sortTimesObj(this.config.data.datasets[i].data);
        return true;
      }
    }
    
    return false;
  }
  getStartFactor(arr:any, stratName:string, factor:string){
    for(var i = 0; i < arr.length; i++){
      if(arr[i]["name"] === stratName){
        return arr[i][factor];
      }
    }
  }

  /* profit margin = -[(SA - NA) / SA] */
  checkExitCondition(loss: number, profit: number, startAmount:number, currentAmount:number){
    // console.log("Start amount was: " + startAmount);
    // console.log("Current amount is: " + currentAmount);
    // console.log("Profit margin is: " + -((startAmount-currentAmount)/startAmount));
    // console.log("Loss: " + loss);
    // console.log("Profit: " + profit);
    console.log(startAmount);
    console.log(currentAmount);
    loss = -loss;
    let profitMargin = -((startAmount - currentAmount)/startAmount);

    console.log("Profit margin: " + profitMargin);

    // is a loss => check if it goes below the loss percentage
    if(profitMargin >= profit || profitMargin <= loss){
      return true;
    }
    return false;
  }
  handleExit(exit, name, arr){
    if(exit){
      for(var i = 0; i < arr.length; i++){
        if(arr[i]["strategyName"] === name){
          arr[i]["active"] = false;
          this.httpClient.put('http://172.31.14.93:8080/api/StrategyInfo/',arr[i]).subscribe((data)=>{
            console.log("Turned strategy off");
          });
        }
      }
    }
  }
  sortTimesObj(times:any){
    times.sort((a: any, b: any)=>{
      let newA : any;
      let newB : any;
      newA = this.timeToNumber(a["x"]);
      newB = this.timeToNumber(b["x"]);
      return newA - newB;
    })
  }
  sortTimes(times:any){
    times.sort((a: any, b: any)=>{
      let newA : any;
      let newB : any;
      newA = this.timeToNumber(a);
      newB = this.timeToNumber(b);
      return newA - newB;
    })
  }
  timeToNumber(str:string){
    // 12:53:57 => 12.5357
    let arr = str.split(":");
    let conv = arr[0] + "." + arr[1] + "" + arr[2];
    return conv;
  }
}
