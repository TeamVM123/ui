import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionUIComponent } from './transaction-ui.component';

describe('TransactionUIComponent', () => {
  let component: TransactionUIComponent;
  let fixture: ComponentFixture<TransactionUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
